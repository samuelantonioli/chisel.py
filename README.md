# chisel.py
simple static page generator for py2.x  
no features, you can `@step` it in, i.e tag support.

## forked
forked from  
  <https://github.com/dz/chisel>  
with code from enhanced fork (rss support)  
  <https://github.com/ckunte/chisel/>  

## changes:
  - clean config and more options
  - better head parser (see post)
  - clean template interface (write shortcut)
  - error handling
  - rss step (from ckunte)

# post example:

*this* chisel.py parses key/value pairs with a colon as delimiter.  
the **blank line** between head and code is necessary to divide the post.  
it trims the keys/values, so you don't have to adjust it to a specific spacing.

        title   : Hello World
        date : 28.12.2015
        key:value
        
        # Markdown or
        <h1>HTML code</h1>

# steps

chisel.py works with steps, that means every step gets the parsed files  
and the jinja2 environment to compile templates.  
use the decorator `@step` to add a step:

        @step
        def generate_homepage(files, env):
            '''homepage'''
            TPL.write(env, 'home', entries=files[:15])

# what else

chisel.py is awesome, simple but extensible.  
read the source, especially the `config` to see the options  
icon from [WPZOOM](http://www.wpzoom.com)

# dependencies
requires:

  - [jinja2](http://jinja.pocoo.org/docs/)

optional:

  - [PyRSS2Gen](https://pypi.python.org/pypi/PyRSS2Gen)
  - [Python Markdown](https://pypi.python.org/pypi/Markdown)
  - your own filters (i.e. [SmartyPants](https://pypi.python.org/pypi/smartypants) etc.)
