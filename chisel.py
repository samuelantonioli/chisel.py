#!/usr/bin/python
# chisel.py
# > forked by Samuel A.
# > simple static page generator
# > python2
# forked from
#   <https://github.com/dz/chisel>
# with code from enhanced fork (rss support)
#   <https://github.com/ckunte/chisel/>
# changes:
#   - clean config and more options
#   - better head parser (see post)
#   - clean template interface (write shortcut)
#   - error handling
#   - rss step (from ckunte)
# requires:
#   jinja2
# optional:
#   PyRSS2Gen
#   markdown
'''[Example Post]
  title   : Hello World
  date : 28.12.2015
  key:value

# Markdown or
<h1>HTML code</h1>'''
import sys, time, os, datetime, jinja2
try:
    # delete this try/except block if you need markdown
    import markdown
except ImportError:
    print '[*] markdown missing, uses plain input'
    class markdown:
        '''imitate markdown module'''
        @staticmethod
        def markdown(x,y=None): return x

config={
    #end with slash
    'src': './blog/',
    'dst': './export/',
    'tpd': './templates/a/',
    #template setup
    'n'  : 15, #number of elements (homepage)
    'opt': {},
    'tpl': {
        #tpl-name   : (template, output),
        'home'      : ('home.html', 'home.html'),
        'detail'    : ('detail.html', 'detail.html'),
        'archive'   : ('archive.html', 'archive.html'),
    },
    #comment characters in header section
    'cmt': ('#','/','<','-','>'),
    #time formatting
    'tin'   : '%d.%m.%Y', #time in
    'tout'  : '%d.%m.%Y', #time output (rendered)
    #format markup
    'suffix': True, #False if server supports rewrite (.html->no ext)
    'format': lambda t: markdown.markdown(t,['footnotes']),
    'filter': None, # lambda file: ... filter files
    'url'   : lambda y,m,d,f: '/'.join(
        (y,m,d,f) #year, month, day, filename
    ),
    #rss feeds, 'rss' blank or None to deactivate
    'rss'       : 'http://www.example.org/',
    'rsstitle'  : 'example.org RSS',
    'rssdesc'   : 'RSS Feeds',
    'author'    : '',
}
N       = config['n']
STEPS   = []
def write_file(url, data):
    path = config['dst'] + url
    dirs = os.path.dirname(path)
    if not os.path.isdir(dirs):
        os.makedirs(dirs)
    fil = open(path, 'w')
    fil.write(data.encode('UTF-8'))
    fil.close()

class TPL:
    '''nicer interface'''
    @staticmethod
    def check(x):
        if not (config['tpl'].get(x) and len(config['tpl'][x])==2):
           raise ValueError('template '+str(x))
    @staticmethod
    def tpl(x):
        TPL.check(x)
        return config['tpl'][x][0]
    @staticmethod
    def out(x):
        TPL.check(x)
        return config['tpl'][x][1]
    @staticmethod
    def write(e,x,**opts):
        '''write_file shortcut'''
        TPL.check(x)
        t=e.get_template(TPL.tpl(x))
        write_file(TPL.out(x),t.render(**opts))

def step(func):
    def wrapper(*args, **kwargs):
        print '[*] '+ func.__doc__ ,
        func(*args, **kwargs)
        print '_'
    STEPS.append(wrapper)
    return wrapper

def post(s):
    '''parse post header'''
    head,html=s.split('\n'*2,1)
    head=reduce(
        lambda x,y:
            x+'\n'+y if not y.lstrip().startswith(config['cmt']) else x,
        head.splitlines()
    )
    conf={}
    for line in head.splitlines():
        try:
            k,v=line.split(':',1)
        except Exception: continue
        conf[k.lower().strip()]=v.strip()
    return (conf, html)

def get_tree(source):
    files = []
    for root, ds, fs in os.walk(source):
        for name in fs:
            if name[0]=='.': continue
            path = os.path.join(root, name)
            f = open(path, 'rU')
            c,p = post(f.read())
            try:
                #required keys
                title = c['title']
                date = time.strptime(c['date'], config['tin'])
            except Exception,k:
                print ' '*4+'[!]',k,'in',path[-70:]
                continue
            y,m,d = map(lambda x:str(x), date[:3])
            fil = os.path.splitext(name)[0]
            fil+= '.html' if config.get('suffix') else ''
            files.append({
                'title': title,
                'epoch': time.mktime(date),
                'content': config['format'](p.decode('UTF-8')),
                'url': (config.get('url') and config['url'](y,m,d,fil)) or '/'.join(
                    (y,m,d,fil)
                 ),
                'date': time.strftime(config['tout'], date),
                'date_object': date,
                'year': int(y),
                'month': int(m),
                'day': int(d),
                'filename': name,
            })
            f.close()
    return files

def init_modules():
    try:
        import PyRSS2Gen
    except ImportError:
        print '[*] PyRSS2Gen missing'
        PyRSS2Gen = None
    if PyRSS2Gen and config.get('rss') and \
       config.get('rsstitle') and config.get('rssdesc'):
        @step
        def gen_rss(f,e):
            '''rss'''
            rss = PyRSS2Gen.RSS2(
                title=config.get('rsstitle'),
                description=config.get('rssdesc'),
                link=config.get('rss'),
                items=[],
                lastBuildDate=datetime.datetime.now()
            )
            for fil in f: #f[:N]:
                rss.items.append(PyRSS2Gen.RSSItem(
                    title=fil['title'],
                    link=config.get('rss')+fil['url'],
                    description=fil['content'],
                    guid=PyRSS2Gen.Guid(config.get('rss')+fil['url']),
                    pubDate=datetime.datetime(
                        fil['year'], fil['month'], fil['day']
                    ),
                    author=fil.get('author') or config.get('author') or ''
                ))
            rss.write_xml(open(config['dst']+'rss.xml', 'w'))
'''
STEPS
  a step function gets all entries and the jinja2
  environment as parameters. the step can't modify
  the files for other steps, but it can compile
  templates with this file object list.
  if you want to filter the files first, use filter
  in config.
'''
@step
def generate_homepage(f, e):
    '''homepage'''
    TPL.write(e, 'home', entries=f[:N])

@step
def master_archive(f, e):
    '''archive'''
    TPL.write(e, 'archive', entries=f)

@step
def detail_posts(f, e):
    '''posts'''
    template = e.get_template(TPL.tpl('detail'))
    #filter(lambda x: not (x.has_key('page') and x.get('page').lower()!='true'),f)
    for fil in f:
        write_file(
            fil['url']+'' if fil['url'][-5:]=='.html' else '.html',
            template.render(entry=fil)
        )

def main():
    print 'chisel.py'
    init_modules()
    print '[*] load files'
    def cmp_entries(x, y):
        r = cmp(-x['epoch'], -y['epoch'])
        return -cmp(x['filename'], y['filename']) if r==0 else r
    files = sorted(get_tree(config['src']), cmp=cmp_entries)
    if config.get('filter'):
        files = filter(config['filter'], files)
    if not len(files):
        print '\n[*] no files'
        return
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(config['tpd']),
        **config['opt']
    )
    print '[*]',len(files),'loaded'
    print '[*]',len(STEPS),'steps'
    for step in STEPS:
        print ' '*3,
        try:
            step(files, env)
        except Exception,e:
            print '\n'+' '*8+'[!] error:',e

if __name__ == '__main__': sys.exit(main())
